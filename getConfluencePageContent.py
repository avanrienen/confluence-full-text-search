# Python script: getConfluencePageContent
# ---------------------------------------
# Friendly warning: This script is provided "as is" and without any guarantees.
# I developed it to solve a specific problem.
# I'm sharing it because I hope it will be useful to others too.
# If you have any improvements to share, please let me know.
#
# Author: Sarah Maddox
# Source: https://bitbucket.org/sarahmaddox/confluence-full-text-search
# Usage guide: http://ffeathers.wordpress.com/2013/04/20/confluence-full-text-search-using-python-and-grep/
#
# A Python script that gets the content of all pages in a given Confluence space.
# It puts the content of each page into a separate text file, in a given directory.
# The content is in the form of the Confluence "storage format",
# which is a type of XML consisting of HTML with Confluence-specific elements.
#
# This script works with Python 3.2.3

import xmlrpc.client
import os
import re

# Get from input: Confluence URL, username, password, space key, output directory

print("G'day! I'm the getConfluencePageContent script.\nGive me a Confluence space, and I'll give you the content of all its pages.\n")
site_URL = input("Confluence site URL (exclude final slash): ")
username = input("Username: ")
pwd = input("Password: ")
spacekey = input("Space key: ")
output_directory = input("Output directory (relative to current, exclude initial slash, example 'output'): ")

# Create the output directory
# os.mkdir("../output")
output_path = "../" + output_directory
os.mkdir(output_path)

# Log in to Confluence
server = xmlrpc.client.ServerProxy(site_URL + "/rpc/xmlrpc")
token = server.confluence2.login(username, pwd)

# Get all the pages in the space
pages_list = server.confluence2.getPages(token, spacekey)

# For each page, get the content of the page and write it out to a file
for page in pages_list:
    # Get the content of the page
    page_content = server.confluence2.getPage(token, page["id"])
    # File name is equal to page name without special characters, plus page ID.
    # Use a regular expression (re) to strip non-alphanum characters from page name.
    page_name = page["title"]
    page_id = page["id"]
    page_name_qualified = (re.sub(r'([^\s\w]|_)+', '', page_name)) + "-" + page_id
    # Open the output file for writing. Will overwrite existing file.
    # File is in the required output directory.
    page_file = open(os.path.join(output_path, page_name_qualified), "w+")
    # Write a line containing the URL of the page, marked with asterisks for easy grepping
    page_file.write("**" + page["url"] + "**\n")
    # Write the page content to the file, after removing any weird characters such as a BOM
    page_content_unsafe = page_content["content"]
    safe_content = str(page_content_unsafe.encode('ascii', 'xmlcharrefreplace'))
    # Remove unwanted characters at beginning and end
    safe_content = str.lstrip(safe_content, "b'")
    safe_content = str.rstrip(safe_content, "'")
    page_file.write(safe_content)
    page_file.close()
print("All done! I've put the results in this directory: ", output_directory)


